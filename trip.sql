-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.7.13 - MySQL Community Server (GPL)
-- ОС Сервера:                   Win64
-- HeidiSQL Версия:              8.0.0.4396
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры базы данных trip
CREATE DATABASE IF NOT EXISTS `trip` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `trip`;


-- Дамп структуры для таблица trip.directions
CREATE TABLE IF NOT EXISTS `directions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `from` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `to` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `from` (`from`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы trip.directions: ~43 rows (приблизительно)
DELETE FROM `directions`;
/*!40000 ALTER TABLE `directions` DISABLE KEYS */;
INSERT INTO `directions` (`id`, `from`, `to`, `created_at`, `updated_at`) VALUES
	(1, 'Odessa', 'Kiev', '2017-04-22 12:49:51', '2017-04-22 12:49:51'),
	(2, 'Odessa', 'Zaporizhzhya', '2017-04-22 12:50:57', '2017-04-22 12:50:57'),
	(3, 'Odessa', 'Kishenev', '2017-04-22 12:51:16', '2017-04-22 12:51:16'),
	(4, 'Odessa', 'Lvov', '2017-04-22 12:51:30', '2017-04-22 12:51:30'),
	(5, 'Odessa', 'Dnepr', '2017-04-22 12:52:45', '2017-04-22 12:52:45'),
	(6, 'Odessa', 'Kiev', '2017-04-22 12:53:15', '2017-04-22 12:53:15'),
	(7, 'Odessa', 'Minsk', '2017-04-22 12:53:39', '2017-04-22 12:53:39'),
	(8, 'Odessa', 'Kiev', '2017-04-22 12:54:24', '2017-04-22 12:54:24'),
	(9, 'Odessa', 'Dnepr', '2017-04-22 12:54:36', '2017-04-22 12:54:36'),
	(10, 'Odessa', 'Kharkiv', '2017-04-22 12:54:57', '2017-04-22 12:54:57'),
	(11, 'Dnepr', 'Kharkiv', '2017-04-22 12:55:12', '2017-04-22 12:55:12'),
	(12, 'Dnepr', 'Zaporizhzhya', '2017-04-22 12:55:27', '2017-04-22 12:55:27'),
	(13, 'Dnepr', 'Lvov', '2017-04-22 12:56:01', '2017-04-22 12:56:01'),
	(14, 'Dnepr', 'Kiev', '2017-04-22 12:56:17', '2017-04-22 12:56:17'),
	(15, 'Dnepr', 'Kiev', '2017-04-22 12:56:32', '2017-04-22 12:56:32'),
	(16, 'Dnepr', 'Kiev', '2017-04-22 12:57:01', '2017-04-22 12:57:01'),
	(17, 'Dnepr', 'Kiev', '2017-04-22 12:57:22', '2017-04-22 12:57:22'),
	(18, 'Dnepr', 'Odessa', '2017-04-22 12:57:50', '2017-04-22 12:57:50'),
	(19, 'Dnepr', 'Odessa', '2017-04-22 12:57:58', '2017-04-22 12:57:58'),
	(20, 'Kiev', 'Odessa', '2017-04-22 12:58:32', '2017-04-22 12:58:32'),
	(21, 'Kiev', 'Dnepr', '2017-04-22 12:58:44', '2017-04-22 12:58:44'),
	(22, 'Kiev', 'Dnepr', '2017-04-22 12:58:52', '2017-04-22 12:58:52'),
	(23, 'Kiev', 'Dnepr', '2017-04-22 12:59:06', '2017-04-22 12:59:06'),
	(24, 'Kiev', 'Dnepr', '2017-04-22 12:59:18', '2017-04-22 12:59:18'),
	(25, 'Kiev', 'Odessa', '2017-04-22 12:59:50', '2017-04-22 12:59:50'),
	(26, 'Kiev', 'Odessa', '2017-04-22 12:59:58', '2017-04-22 12:59:58'),
	(27, 'Kiev', 'Kharkiv', '2017-04-22 13:00:11', '2017-04-22 13:00:11'),
	(28, 'Lvov', 'Kharkiv', '2017-04-22 13:00:42', '2017-04-22 13:00:42'),
	(29, 'Lvov', 'Zaporizhzhya', '2017-04-22 13:00:56', '2017-04-22 13:00:56'),
	(30, 'Lvov', 'Kiev', '2017-04-22 13:01:27', '2017-04-22 13:01:27'),
	(31, 'Lvov', 'Kiev', '2017-04-22 13:01:38', '2017-04-22 13:01:38'),
	(32, 'Lvov', 'Kiev', '2017-04-22 13:01:41', '2017-04-22 13:01:41'),
	(33, 'Lvov', 'Kiev', '2017-04-22 13:01:52', '2017-04-22 13:01:52'),
	(34, 'Lvov', 'Odessa', '2017-04-22 13:02:00', '2017-04-22 13:02:00'),
	(35, 'Lvov', 'Odessa', '2017-04-22 13:02:04', '2017-04-22 13:02:04'),
	(36, 'Lvov', 'Odessa', '2017-04-22 13:02:08', '2017-04-22 13:02:08'),
	(37, 'Zaporizhzhya', 'Odessa', '2017-04-22 13:02:54', '2017-04-22 13:02:54'),
	(38, 'Zaporizhzhya', 'Dnepr', '2017-04-22 13:03:06', '2017-04-22 13:03:06'),
	(39, 'Lvov', 'Praga', '2017-04-22 13:04:15', '2017-04-22 13:04:15'),
	(40, 'Praga', 'Berlin', '2017-04-22 13:04:33', '2017-04-22 13:04:33'),
	(41, 'Berlin', 'London', '2017-04-22 13:04:51', '2017-04-22 13:04:51'),
	(42, 'Dnepr', 'Praga', '2017-04-22 13:06:08', '2017-04-22 13:06:08'),
	(43, 'Dnepr', 'Praga', '2017-04-22 13:06:14', '2017-04-22 13:06:14');
/*!40000 ALTER TABLE `directions` ENABLE KEYS */;


-- Дамп структуры для таблица trip.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы trip.migrations: ~4 rows (приблизительно)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2017_04_22_105320_create_directions_table', 1),
	(4, '2017_04_22_105610_create_tickets_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;


-- Дамп структуры для таблица trip.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы trip.password_resets: ~0 rows (приблизительно)
DELETE FROM `password_resets`;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;


-- Дамп структуры для таблица trip.tickets
CREATE TABLE IF NOT EXISTS `tickets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `direction_id` int(11) NOT NULL,
  `transport_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transport_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `info` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `direction_id` (`direction_id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы trip.tickets: ~43 rows (приблизительно)
DELETE FROM `tickets`;
/*!40000 ALTER TABLE `tickets` DISABLE KEYS */;
INSERT INTO `tickets` (`id`, `direction_id`, `transport_type`, `transport_number`, `seat`, `info`, `created_at`, `updated_at`) VALUES
	(1, 1, 'Train', 'TR-12', '99A', 'Lorem ipsum dolor', '2017-04-22 12:49:51', '2017-04-22 12:49:51'),
	(2, 2, 'Train', 'TR-132', '93', 'Lorem ipsum dolor', '2017-04-22 12:50:57', '2017-04-22 12:50:57'),
	(3, 3, 'Train', 'TR-13', '23', 'Lorem ipsum dolor', '2017-04-22 12:51:16', '2017-04-22 12:51:16'),
	(4, 4, 'Train', 'TR-77', '73', 'Lorem ipsum dolor', '2017-04-22 12:51:30', '2017-04-22 12:51:30'),
	(5, 5, 'Plain', 'PL-1177', '7', 'Lorem ipsum dolor', '2017-04-22 12:52:45', '2017-04-22 12:52:45'),
	(6, 6, 'Plain', 'PL-2377', '37', 'Lorem ipsum dolor', '2017-04-22 12:53:15', '2017-04-22 12:53:15'),
	(7, 7, 'Plain', 'PL-221', '77', 'Lorem ipsum dolor', '2017-04-22 12:53:39', '2017-04-22 12:53:39'),
	(8, 8, 'Bus', 'A121', '12', 'Lorem ipsum dolor', '2017-04-22 12:54:24', '2017-04-22 12:54:24'),
	(9, 9, 'Bus', 'D33', '32', 'Lorem ipsum dolor', '2017-04-22 12:54:36', '2017-04-22 12:54:36'),
	(10, 10, 'Bus', 'D23', '43', 'Lorem ipsum dolor', '2017-04-22 12:54:57', '2017-04-22 12:54:57'),
	(11, 11, 'Bus', 'D33', '43', 'Lorem ipsum dolor', '2017-04-22 12:55:12', '2017-04-22 12:55:12'),
	(12, 12, 'Bus', 'Z99', '11', 'Lorem ipsum dolor', '2017-04-22 12:55:27', '2017-04-22 12:55:27'),
	(13, 13, 'Plain', 'PL-9921', '34', 'Lorem ipsum dolor', '2017-04-22 12:56:01', '2017-04-22 12:56:01'),
	(14, 14, 'Plain', 'PL-432', '31', 'Lorem ipsum dolor', '2017-04-22 12:56:17', '2017-04-22 12:56:17'),
	(15, 15, 'Bus', 'B99', '54', 'Lorem ipsum dolor', '2017-04-22 12:56:32', '2017-04-22 12:56:32'),
	(16, 16, 'Train', 'TR-0123', '123', 'Lorem ipsum dolor', '2017-04-22 12:57:01', '2017-04-22 12:57:01'),
	(17, 17, 'Train', 'TR-223', '35', 'Lorem ipsum dolor', '2017-04-22 12:57:22', '2017-04-22 12:57:22'),
	(18, 18, 'Train', 'TR-25', '55', 'Lorem ipsum dolor', '2017-04-22 12:57:50', '2017-04-22 12:57:50'),
	(19, 19, 'Train', 'TR-25', '122', 'Lorem ipsum dolor', '2017-04-22 12:57:58', '2017-04-22 12:57:58'),
	(20, 20, 'Train', 'TR-225', '122', 'Lorem ipsum dolor', '2017-04-22 12:58:32', '2017-04-22 12:58:32'),
	(21, 21, 'Train', 'TR-15', '12', 'Lorem ipsum dolor', '2017-04-22 12:58:44', '2017-04-22 12:58:44'),
	(22, 22, 'Train', 'TR-125', '121', 'Lorem ipsum dolor', '2017-04-22 12:58:52', '2017-04-22 12:58:52'),
	(23, 23, 'Bus', 'B-4', '33', 'Lorem ipsum dolor', '2017-04-22 12:59:06', '2017-04-22 12:59:06'),
	(24, 24, 'Bus', 'B-434', '32', 'Lorem ipsum dolor', '2017-04-22 12:59:18', '2017-04-22 12:59:18'),
	(25, 25, 'Plain', 'PL-992', '44', 'Lorem ipsum dolor', '2017-04-22 12:59:50', '2017-04-22 12:59:50'),
	(26, 26, 'Plain', 'PL-12', '4', 'Lorem ipsum dolor', '2017-04-22 12:59:58', '2017-04-22 12:59:58'),
	(27, 27, 'Plain', 'PL-55', '14', 'Lorem ipsum dolor', '2017-04-22 13:00:11', '2017-04-22 13:00:11'),
	(28, 28, 'Plain', 'PL-535', '14', 'Lorem ipsum dolor', '2017-04-22 13:00:42', '2017-04-22 13:00:42'),
	(29, 29, 'Plain', 'PL-335', '124', 'Lorem ipsum dolor', '2017-04-22 13:00:56', '2017-04-22 13:00:56'),
	(30, 30, 'Plain', 'PL-35', '24', 'Lorem ipsum dolor', '2017-04-22 13:01:27', '2017-04-22 13:01:27'),
	(31, 31, 'Train', 'TR-01', '24', 'Lorem ipsum dolor', '2017-04-22 13:01:38', '2017-04-22 13:01:38'),
	(32, 32, 'Train', 'TR-01', '14', 'Lorem ipsum dolor', '2017-04-22 13:01:41', '2017-04-22 13:01:41'),
	(33, 33, 'Train', 'TR-61', '141', 'Lorem ipsum dolor', '2017-04-22 13:01:52', '2017-04-22 13:01:52'),
	(34, 34, 'Train', 'TR-32', '141', 'Lorem ipsum dolor', '2017-04-22 13:02:00', '2017-04-22 13:02:00'),
	(35, 35, 'Train', 'TR-32', '14', 'Lorem ipsum dolor', '2017-04-22 13:02:04', '2017-04-22 13:02:04'),
	(36, 36, 'Train', 'TR-32', '143', 'Lorem ipsum dolor', '2017-04-22 13:02:08', '2017-04-22 13:02:08'),
	(37, 37, 'Train', 'TR-99', '99', 'Lorem ipsum dolor', '2017-04-22 13:02:54', '2017-04-22 13:02:54'),
	(38, 38, 'Train', 'TR-29', '21', 'Lorem ipsum dolor', '2017-04-22 13:03:06', '2017-04-22 13:03:06'),
	(39, 39, 'Plain', 'P-1129', '61', 'Lorem ipsum dolor', '2017-04-22 13:04:15', '2017-04-22 13:04:15'),
	(40, 40, 'Plain', 'P-6629', '74', 'Lorem ipsum dolor', '2017-04-22 13:04:33', '2017-04-22 13:04:33'),
	(41, 41, 'Plain', 'P-129', '45', 'Lorem ipsum dolor', '2017-04-22 13:04:51', '2017-04-22 13:04:51'),
	(42, 42, 'Plain', 'PL-999', '87', 'Lorem ipsum dolor', '2017-04-22 13:06:08', '2017-04-22 13:06:08'),
	(43, 43, 'Plain', 'PL-999', '77', 'Lorem ipsum dolor', '2017-04-22 13:06:14', '2017-04-22 13:06:14');
/*!40000 ALTER TABLE `tickets` ENABLE KEYS */;


-- Дамп структуры для таблица trip.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы trip.users: ~0 rows (приблизительно)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
