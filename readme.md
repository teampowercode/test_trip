# Install
1. clone from github
2. run composer install
3. read the description of endpoints
4. make a request (for example REQUEST BODY)

## ENDPOINTS
#####ROUTE:
http://{{ your_domain.com }}/api/route

#####METHOD:
Post

#####REQUEST HEADER:
[Content-Type:application/json , X-Requested-With:XMLHttpRequest]

#####REQUEST BODY:
[
	{"from":"Lvov",            "to":"Zaporizhzhya1",  "transport": "Bus",    "number": "36002",    "seat": "27", "info": "Lorem inpsum"},
    {"from":"Dnepr",           "to":"Kiev",   "transport": "Plain",  "number": "PL-223",   "seat": "36", "info": "Lorem inpsum"},
    {"from":"Zaporizhzhya",    "to":"Dnepr",  "transport": "Train",  "number": "TR-2340",  "seat": "66", "info": "Lorem inpsum"},
    {"from":"Odessa",          "to":"Lvov",   "transport": "Train",  "number": "TR-121",   "seat": "22", "info": "Lorem inpsum"},
    {"from":"Kiev",            "to":"Odessa", "transport": "Train",  "number": "TR-88",    "seat": "42", "info": "Lorem inpsum"}
]


##### ROUTE FILE
YOUR_APLICATION\routes\api.php

##### CONTROLLER
YOUR_APLICATION\app\Http\Controllers\Api\HomeController.php