<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable = [
        'direction_id', 'transport_type', 'transport_number', 'seat', 'info'
    ];
}
