<?php

namespace App\Http\Controllers\Api;

use App\Direction;
use App\Ticket;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{

    public function route(Request $request)
    {
        /**
         * Stack of tickets
         */
        $ticket_stack = $request->all();

        /**
         * Randomize Tickets for testing
         */
//         shuffle($ticket_stack);

        $path = [];
        $endpoints = [];

        foreach ($ticket_stack as $val) {
            /**
             * Check for empty fields
             */
            if ((!isset($val['from']) OR $val['from'] == '') OR ((!isset($val['to']) OR $val['to'] == ''))) {
                $json = [
                    'success' => false,
                    'code'    => 400,
                    'message' => 'Route error',
                ];
                return response()->json($json, 400);
            }

            $path[$val['from']] = $val['to'];

            /**
             * Find start and finish point
             */
            if (isset($endpoints[$val['from']])) {
                unset($endpoints[$val['from']]);
            } else {
                $endpoints[$val['from']] = 'from';
            }

            if (isset($endpoints[$val['to']])) {
                unset($endpoints[$val['to']]);
            } else {
                $endpoints[$val['to']] = 'to';
            }

        }

        $response = [];
        $err = [];

        if (count($endpoints) == 2)
        {
        	/**
        	 * find start point
        	 */

        	$city_a = null;
        	$city_b = null;

	        foreach ($endpoints as $key => $way) {
	            if ($way == 'from') {
	                $city_a = $key;
	            } else {
	            	$city_b = $key;
	            }
	        }

	        if ($city_a && $city_b)
	        {
		        /**
		         * find Directions
		         */
		        $answer = [];

		        while (count($path)) {
		            $city_b = $path[$city_a];
		            $answer[] = ['from' => $city_a, 'to' => $city_b];
		            unset($path[$city_a]);
		            $city_a = $city_b;
		        }

		        /**
		         * get Info for every direction
		         */
		        foreach ($answer as $direction) {
		            foreach ($ticket_stack as $ticket) {
		                if(($ticket['from'] == $direction['from']) && ($ticket['to'] == $direction['to'])) {
		                    $response[] = $ticket;
		                }
		            }
		        }
	        }
	        else
	        {
	        	$err[] = 'Not enough tickets';
	        }
        }
        else
        {
        	$err[] = 'Not enough tickets';
        }

        $json = [
            'success' => count($response),
            'code'    => 200,
            'message' => count($response) ? $response : $err,
        ];
        return response()->json($json, 200);
    }
}